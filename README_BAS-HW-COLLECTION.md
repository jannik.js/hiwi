# Hardware Collection Python Script
Die Idee hinter dem Hardware Collection Skript ist, dass es ein zentrales Skript gibt, welches die Hardwareinformationen sammelt und daraus ein JSON-Output generiert. Dieser kann dann an die unterschiedlichen Endpoints geschickt werden, wie zum Beispiel den BAS (für die iDoIT-Inventarisierung) oder dem slx-admin.

Das Skript sollte also unabhängig von den Endpoints sein und entsprechend keine z. B. BAS spezifischen Funktionen implementiert haben.

## Repository
https://git.openslx.org/openslx-ng/systemd-init.git/tree/modules.d/bas-hw-collect/scripts/collect_hw_info_json.py?h=bas

## Usage
```
usage: collect_hw_info_json.py [-h] [-d] [-u URL] [-uu UUIDURL] [-p] [-l LOCATION] [-s SLOT] [-b BAY] [-c CONTACT] [-n NAME] [-S]

Collects hardware data from different tools and returns it as json.

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           Prints all STDERR messages. (Non critical included)
  -u URL, --url URL     [multiple] If given, a post request with the generated JSON is sent to the given URLs
  -uu UUIDURL, --uuidurl UUIDURL
                        [multiple] Same as -u but UUID in the url is replaced with the actual uuid of the client
  -p, --print           Prints the generated JSON
  -l LOCATION, --location LOCATION
                        Room-/Rackname where the client is located
  -s SLOT, --slot SLOT  The slot number (int) where the client is located in the rack
  -b BAY, --bay BAY     The bay number (int) where the client is located in the slot (segment)
  -c CONTACT, --contact CONTACT
                        [multiple] The idoit_username of the person responsible for this machine
  -n NAME, --name NAME  Name of the client
  -S, --SERVER          Defines the type of the client to be a server
```

### Parameter `-u`
* Schickt den generierten JSON-Output an die gegebene URL (unverändert)

#### Beispiel
Erstellen eines neuen Clients:\
`https://bas.intra.uni-freiburg.de/api/registration/clients`

Updaten eines existierenden Clients:\
`https://bas.intra.uni-freiburg.de/api/registration/clients/FCE263F5-35FF-4FB6-9145-112F2FD994D2`

### Parameter `-uu`
* Ersetzt automatisch "UUID" in der gegebenen URL mit der richtigen UUID des Clients, auf dem das Skript ausgeführt wird
* Kann dafür benutzt werden, um einen Client über den BAS zu updaten, ohne die UUID selber heraus zu suchen
#### Beispiel
1. Man gibt den URL mit dem Parameter `-uu` und dem ganz generischen Wort "UUID" an:\
`-uu https://bas.intra.uni-freiburg.de/api/registration/clients/UUID`
2. Daraufhin ruft das Python Skript (automatisch) die Methode `get_uuid()` auf, in der die UUID des Clients aus dem Pfad `/sys/class/dmi/id/product_uuid` ausgelesen wird z. B. `FCE263F5-35FF-4FB6-9145-112F2FD994D2`
3. Das Wort "UUID" (Ohne "") wird in der URL dann mit der eigentlichen UUID des Clients ersetzt:\
`https://bas.intra.uni-freiburg.de/api/registration/clients/FCE263F5-35FF-4FB6-9145-112F2FD994D2`
4. Der generierte JSON-Output mit den Hardwareinformationen wird an den veränderten URL aus 3. geschickt

