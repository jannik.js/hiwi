# BAS:
### Server
`10.8.102.124`

### Working Directory
`/home/bas/src/`

### Services
`bas-scheduler.service`\
`bas-servers.target`

___
## URLs
### Produktiv System (wird durch haProxy verteilt port 8000-8003)
https://bas.intra.uni-freiburg.de

### Test System
https://bas.intra.uni-freiburg.de:7000

### BAS Doku (unvollständig und mittlerweile bestimmt auch outdated)
http://bas.intra.uni-freiburg.de/documentation

### Tickets
https://lab.ks.uni-freiburg.de/projects/ss18-boot-auswahl-server/issues

___
## Repositories

### Git BAS Frontend/Backend
https://git.ks.uni-freiburg.de/lsfks/projekte/bas.git

### System Init (Minilinux für die Erstregistrierung) incl. HW Collection Python Skript
https://git.openslx.org/openslx-ng/systemd-init.git/log/?h=bas

### HW Collection Python Skript ([README](/README_BAS-HW-COLLECTION.md))
https://git.openslx.org/openslx-ng/systemd-init.git/tree/modules.d/bas-hw-collect/scripts/collect_hw_info_json.py?h=bas


___
## Zertifikat
[Generierung und Antragstellung](/README_BAS-CERTIFICATES.md)

___
## Deployment (10.8.102.124)
Hier gibts bestimmt eine Millionen bessere Methoden, also aus dem Git zu pullen, aber wir sind irgendwie nie dazu gekommen.\
Neuste Version aus dem GIT-Repo ziehen:
```
cd /home/bas/src/
git pull
```

### Webapp
```
cd /home/bas/src/webapp
```
1. Bei Änderungen an den packages (package.json), neue Pakete nachinstallieren
```
npm install
```

2. Neue Version der Webapp bauen. Diese wird automatisch am Ort `../server/public` gebaut (Definiert als `outputDir` in der `/webapp/vue.config.js`).
```
npm run build
```

### Documentation
```
cd /home/bas/src/documentation
```
1. Bei Änderungen an den packages (package.json), neue Pakete nachinstallieren
```
npm install
```

2. Neue Version der Documentation bauen. Diese wird automatisch am Ort `../server/public_documentation` gebaut, wodurch der NodeJS-Server immer die neuste gebaute Version ausliefert. Um den Pfad anzupassen muss die `dest` in der `/documentation/.vuepress/config.js` editiert werden.
```
./node_modules/.bin/vuepress build
```

### Server
```
cd /home/bas/src/server
```
1. Bei Änderungen an den packages (package.json), neue Pakete nachinstallieren
```
npm install
```

2. Sequelize (Datenbank) migration ausführen (Bei neuen migration files).\
Alte Migration-Files sollten nicht editiert werden, falls doch, muss der entsprechende Eintrag aus dem `SequelizeMeta`-Table gelöscht werden, damit die migration neu ausgeführt wird. Gegebenenfalls müssen dann aber die Tabes aus der Migration vorher gelöscht werden. Also aus Kompatibilitätsgründen lieber eine neue Migration-File erstellen und Änderungen an den Tables darin machen.
```
node_modules/.bin/sequelize db:migrate --config config/database.json
```

3. Server restarten + Scheduler restarten
```
systemctrl restart bas-servers.target
```
`/lib/systemd/system/bas-servers.target` handhabt 4 BAS-Instanzen plus den Scheduler, den der BAS zum WoL starten von Clients benötigt.
```
[Unit]
Description=BAS (Boot Auswahl Server) - HTTPS Servers
Requires=bas-server@8000.service bas-server@8001.service bas-server@8002.service bas-server@8003.service bas-scheduler.service

[Install]
WantedBy=multi-user.target
```

___
## [ToDo + Feature Ideen](/README_BAS-FEATURE-IDEAS.md)
