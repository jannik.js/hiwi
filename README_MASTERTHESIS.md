# Masterthesis
### Testsat
`10.4.9.51`

### Produktiv Server
http://10.4.9.51:5000

### Development Server
http://10.4.9.51:3000

### tmux (mit den laufenden Servern) (root)
```
tmux a -t master-js
```

### Working Directory
`/home/js/`

### Masterthesis
https://drive.google.com/file/d/11cDJruM2HfKkpurXGHJGS9d_cwgu26ZE/view?usp=sharing

### Folien
https://docs.google.com/presentation/d/1JoF12bbCpswteuSePVLBHlAktksnBkQzXLVsGSj3KG0/edit?usp=sharing

### Schaubilder (draw.io)
https://drive.google.com/file/d/1g0_0EgTbjDcGQDnBZ0VojqD6WhxOqJ7H/view?usp=sharing

### README (Erklärungen einrichtung/server start)
https://gitlab.com/jannik.js/websuite/-/blob/master/README.md

___
## Repositories

### webSuite
https://gitlab.com/jannik.js/websuite/

### webSuite frontend
https://gitlab.com/jannik.js/websuite/-/tree/master/src/webapp

### webSuite backend
https://gitlab.com/jannik.js/websuite/-/tree/master/src/server

### guacamole-lite
https://gitlab.com/jannik.js/websuite/-/tree/master/src/guacamole-lite

### Assets (Logo Illustrator Files, Mockups etc.)
https://gitlab.com/jannik.js/websuite/-/tree/master/assets

### mltk
https://git.openslx.org/openslx-ng/mltk.git/log/?h=remote-edit-vm

### slx-admin
https://git.openslx.org/openslx-ng/slx-admin.git/log/?h=remote-edit-vm

### Taskmanger
https://git.openslx.org/openslx-ng/tmlite-bwlp.git/log/?h=remote-edit-vm

### vmChooser
https://git.openslx.org/openslx-ng/vmchooser2.git/log/?h=remote-edit-vm

### slxgreeter
https://git.openslx.org/openslx-ng/slxgreeter.git/log/?h=remote-edit-vm