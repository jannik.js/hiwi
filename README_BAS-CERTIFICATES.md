# Serverzertifikat Generierung und Antragstellung:
`bas.intra.uni-freiburg.de`

## Schlüsselpaar generieren (4096 Bit)
1. 4096-Bit Private Key generieren:
```bash
openssl genrsa -out /home/bas/certificates/20XX_privkey.pem 4096
```
2. Den Key der `root`-Gruppe zuweisen
```bash
sudo chown root:root 20XX_privkey.pem
```

## Zertifikatsantrag generieren
1. Konfigurationsdatei anlegen
```bash
[ req ]
default_bits           = 4096
distinguished_name     = req_distinguished_name
prompt                 = no

[ req_distinguished_name ]
C                      = DE
ST                     = Baden-Wuerttemberg
L                      = Freiburg im Breisgau
O                      = Albert-Ludwigs-Universitaet Freiburg
OU                     = Rechenzentrum
CN                     = bas.intra.uni-freiburg.de
```
2. Zertifikats-Request erstellen
```bash
openssl req -new -sha256 -key /home/bas/certificates/20XX_privkey.pem -out 20XX_request.csr -config /home/bas/certificates/request_config
```

## Zertifikatsantrag stellen
1. Im [UNI-FR CA Webinterface](https://pki.pca.dfn.de/uni-freiburg-ca-g2/pub) unter "Serverzertifikat" die Daten ausfüllen
```
PKCS#10-Zertifikatantrag: 20XX_request.csr
Zertifikatsprofil: Web Server
Name:
E-Mail:
Institut/Abteilung: Rechenzentrum
```
2. Antragsformular herunterladen und unterschreiben
3. Antrag an Tom Minnich übergeben/senden <minnich@rz.uni-freiburg.de>
4. Wenn der Antrag geprüft und genehmigt wurde, wird das Zertifikat an die im Formular angegebene E-Mail geschickt

# BAS Zertifikat ersetzen
## Fullchain
Der BAS benötigt die komplette Zertifikatskette, welche sich an folgendem Ort befinden muss:
```
/server/bin/fullchain.pem
```
Die Fullchain setzt sich zusammen aus der [DFN-Zertifikatskette](https://pki.pca.dfn.de/dfn-ca-global-g2/pub/cacert/chain.txt) und dem beantragten BAS-Zertifikat.
```bash
T-TeleSec GlobalRoot Class 2
└── DFN-Verein Certification Authority 2
    └── DFN-Verein Global Issuing CA
        └── bas.intra.uni-freiburg.de
```
Das sollte am Ende dann folgendermaßen aussehen:
```
subject= /C=DE/S=Baden-Wuerttemberg/L=Freiburg im Breisgau/O=Albert-Ludwigs-Universitaet Freiburg/OU=Rechenzentrum/CN=bas.intra.uni-freiburg.de
-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
subject= /C=DE/O=Verein zur Foerderung eines Deutschen Forschungsnetzes e. V./OU=DFN-PKI/CN=DFN-Verein Global Issuing CA-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
subject= /C=DE/O=Verein zur Foerderung eines Deutschen Forschungsnetzes e. V./OU=DFN-PKI/CN=DFN-Verein Certification Authority 2
-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
subject= /C=DE/O=T-Systems Enterprise Services GmbH/OU=T-Systems Trust Center/CN=T-TeleSec GlobalRoot Class 2
-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
```
Die Zeilen zwischen den `END`- und `BEGIN`-Zeilen dienen nur zur Übersicht und werden von openssl ignoriert.

## Private Key
Der BAS benötigt den Private Key, welcher für den Zertifikatsrequest verwendet wurde.
Der Key muss im `.pem`-Format an folgender stelle liegen:
```
/server/bin/privkey.pem
```
WICHTIG: Sicherstellen, dass privkey dem User und der Gruppe bas gehört. Falls dem nicht so ist, folgenden Befehl ausführen:
```bash
sudo chown bas:bas /home/bas/src/server/bin/privkey.pem
```

## BAS Neustarten
Nach dem austausch der beiden Schlüssel, muss der BAS einmal Neugestartet werden. Alle 4 Instanzen können gleichzeitig mittels folgendem Befehl neugestartet werden:
```bash
sudo systemctl restart bas-servers.target
```

## Embedded iPXE
Damit die Clients vom BAS booten können, muss die gesamte Zertifikatskette in das embedded iPXE integriert werden.

Zum builden vom iPXE wird die Fullchain genommen, mit welcher der BAS startet (`/server/bin/`). Diese sollte nach den obigen Schritten und einem neuladen der Seite bereits im `Embedded certificate` angezeigt werden.

Danach muss das iPXE neu gebaut und als neuer default (Grüne Flagge) gesetzt werden.

WICHTIG: Es reicht nicht einfach, den `BUILD IPXE`-Button zu drücken. Ein kompletter rebuild ist notwendig, weshalb vorher der `CLEAN IPXE`-Buttion gedrückt werden muss.

## HAProxy
HAProxy braucht ebenfalls die neue Zertifikatskette + PrivateKey. Dazu hängt man an die `Fullchain` einfach den `PrivKey` dran und speicher das ganze unter folgendem Pfad ab:
```bash
/etc/haproxy/certs/bas.pem
```
Das ganze sollte am Ende wie folgt aussehen:
```bash
subject= /C=DE/S=Baden-Wuerttemberg/L=Freiburg im Breisgau/O=Albert-Ludwigs-Universitaet Freiburg/OU=Rechenzentrum/CN=bas.intra.uni-freiburg.de
-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
subject= /C=DE/O=Verein zur Foerderung eines Deutschen Forschungsnetzes e. V./OU=DFN-PKI/CN=DFN-Verein Global Issuing CA-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
subject= /C=DE/O=Verein zur Foerderung eines Deutschen Forschungsnetzes e. V./OU=DFN-PKI/CN=DFN-Verein Certification Authority 2
-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
subject= /C=DE/O=T-Systems Enterprise Services GmbH/OU=T-Systems Trust Center/CN=T-TeleSec GlobalRoot Class 2
-----BEGIN CERTIFICATE-----
[...]
-----END CERTIFICATE-----
-----BEGIN RSA PRIVATE KEY-----
[...]
-----END RSA PRIVATE KEY-----
```
WICHTIG: Danach muss HAProxy restarted werden. Das kann mittels folgendem Befehl gemacht werden:
```bash
sudo systemctl restart haproxy.service
```

# Quellen:
* [Serverzertifikat](https://www.wiki.uni-freiburg.de/rz/doku.php?id=serverzertifikat)
* [Erstellen mit openssl](https://www.wiki.uni-freiburg.de/rz/doku.php?id=opensslcert)