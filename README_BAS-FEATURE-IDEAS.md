# TODO Liste + Feature Ideen

## iPXE Builder
Hier war generell mal die Überlegung eine bessere IPC zwischen den Instanzen zu schaffen (z. B. via redis)
* Build-Button disablen, wenn der Prozess gerade schon läuft (über mehrere Instanzen hinweg)
* Cancel-Button während dem Prozess `server/lib/shell.js` cancel-Methode implementieren (Button ist da, "nur" die Backend-Methode fehlt noch)
* Wenn eine Datei noch nicht gespeichert wurde, sollte sich das Icon farblich ändern, um das zu signalisieren

## IP Change erkennen
Wenn z. B. ein Testrechner vom 10.21.9.83 Netz in das 10.3.9.83 (Testbench Netz) umgezogen wird\
* Log eintrag?
* (optionaler) Override z.b. mittels ipxe menu?
* Neue erstregistrierung?


## Registration.ipxe script
Befindet sich in der BAS Ordnerstruktur `server/ipxe/registration.ipxe`
* Nach dem Chain bei z.b. der Automatic registration kann es zu timeouts kommen, wenn z.b. idoit nicht mehr erreichbar ist
   --> || chain configloader?
* Sollte wenn möglich auf Full-HD skalieren

## Groups / Clients
### Delete clients Frontend
Beim Löschen gibt es im Frontend eine Auswahl, aus welchen Backends das Objekt auch noch gelöscht werden soll
* Aktuell werden immer alle backends als option angezeigt
* Besser: Nur die, für die es auch eine Verknüpfung zu dem Objekt gibt, die anderen dann ausgegraut
### Pfad für Parents in Group/Clients
* Eine Pfadanzeige wie in manchen Tables wäre Praktisch
* Allerdings kann eine Group oder ein Client mehrere Pfäde haben, da man eine Group ja in mehreren Gruppen hinzufügen kann
* Hinzufügen zu mehreren Gruppen war erwünscht, weil man so zum einen die Gebäudestruktur vom HisInOne resp. iDoIT übernehmen kann und zusätzlich Gruppen erstellen kann, die ein einfacheres Management ermöglichen wie zum Beispiel eine Gruppe mit allen Racks

## DHCP - Infoblox
* Was passiert, wenn der DHCP voll ist und keine weiteren freien ips mehr vergeben kann?
* Testen, was bei der BAS Registrierung dann passiert

## External Backends
* Stepper entfernen, besser alle Einstellungen in ein Popup machen
* Wenn man den check connection button drück, sollten die hilfetexte verschwinden!


## iPXE
* In den dynamisch generierten ipxe scripts kann kein Port angegeben werden, da ipxe keinen handshake mit ports im url machen kann
* Daher müssen ports z. B. im haproxy immer manuell weitergeleitet werden...
  * In unserem Fall (UNI Freiburg):
  * Soll ein Testclient auf den Testserver umgeleitet werden, muss im HAProxy eine entsprechende Regel gemacht werden, welche die Anfragen der ClientIP auf Port weiterleitet
  ```
    acl test-machine src 10.21.9.200
    use_backend bas-server-test if test-machine
  ```

* Das sollte irgendwo in der Doku festgehalten werden

## Failsave
Aktuell gibt es keinerlei Failsave, weder von der DB, noch einen zweiten (Physikalischen) BAS-Server
* Failsafe mit zweitem Server
* Wenn Server 1 offline muss Server 2 die IP von Server 1 annehmen und alle Anfragen übernehmen
* Und dann auch irgendwann wieder zurückstellen
* Eine ReadOnly DB Kopie, in der im Zweifel zurückgefriffen werden kann wäre auch sinnvoll

## iPXE Configuration
* Die Events vom Eventmanger sollen hier ebenfalls aufgelistet werden, welche die Config zugewiesen haben
